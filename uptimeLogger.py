#!/bin/env python3

import subprocess
import time

# this is the path to the log file (a string representing a filesystem path)
FILE_PATH = "uptime"
# this is the delay between uptime calls (a float representing the delay in seconds)
POLL_DELAY = 2

"""
    This calls the Linux uptime utility and returns the response.
"""
def getUptime():
    # call Linux uptime utility
    process = subprocess.run(["uptime", "--pretty"], stdout=subprocess.PIPE, stdin=subprocess.DEVNULL, stderr=subprocess.PIPE)
    # return the stdout and stderror of the subprocess
    return process.stdout.decode(), process.stderr.decode()

"""
    This writes the text argument to a file located at filePath
"""
def writeFile(filePath, text):
    # this opens the a file at filePath and overwrites the contents with the text variable
    with open(filePath, "w") as targetFile:
        targetFile.write(text)

"""
    This repeatedly fetches the computer's uptime and saves it in a file.
"""
def main():
    # enter main loop
    while True:
        # get uptime from system
        output, error = getUptime()
        # if any errors occur, write them and exit
        if error != '':
            print(error)
            text = "The subprocess call failed.\nSTDOUT: %s\nSTDERR: %s\n" % (output, error)
            writeFile(FILE_PATH, text)
            exit()
        # log the uptime
        writeFile(FILE_PATH, output)
        # wait before the next poll
        time.sleep(POLL_DELAY)

# run the program
try:
    main()
except KeyboardInterrupt:
    exit()